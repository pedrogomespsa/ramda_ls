#!/bin/bash
type createdb >/dev/null 2>&1 || { echo >&2 "You need to install postgres.  Aborting."; exit 1; }
CREATE TABLE public.github_users_langs (
	id bigserial NOT NULL,
	id_user int8 NOT NULL,
	lang_name varchar NOT NULL,
	default_lang bool NOT NULL DEFAULT false,
	PRIMARY key (id),
	FOREIGN KEY (id_user) REFERENCES github_users (id)
);                               


INSERT INTO public.github_users_langs
(id_user, lang_name, default_lang)
VALUES(1, 'Portuguese', true);



INSERT INTO public.github_users_langs
(id_user, lang_name, default_lang)
VALUES(1, 'English', false);


INSERT INTO public.github_users_langs
(id_user, lang_name, default_lang)
VALUES(2, 'English', true);


INSERT INTO public.github_users_langs
(id_user, lang_name, default_lang)
VALUES(2, 'Portuguese', false);


