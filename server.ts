const pgPromise = require('pg-promise');
const R         = require('ramda');
const request   = require('request-promise');

// Limit the amount of debugging of SQL expressions
const trimLogsSize : number = 200;

// Database interface
interface DBOptions
  { host      : string
  , database  : string
  , user?     : string
  , password? : string
  , port?     : number
  };

// Actual database options
const options : DBOptions = {
  user: 'postgres',
  password: 'postgres',
  host: 'localhost',
  database: 'lovelystay_test',
  port:5432,
};

console.info('Connecting to the database:',
  `${options.user}@${options.host}:${options.port}/${options.database}`);

const pgpDefaultConfig = {
  promiseLib: require('bluebird'),
  // Log all querys
  query(query) {
    console.log('[SQL   ]', R.take(trimLogsSize,query.query));
  },
  // On error, please show me the SQL
  error(err, e) {
    if (e.query) {
      console.error('[SQL   ]', R.take(trimLogsSize,e.query),err);
    }
  }
};

interface GithubUsers
  { id : number
  };

const pgp = pgPromise(pgpDefaultConfig);
const db = pgp(options);


const opt = process.argv[2];

const listuser = () =>{
  db.manyOrNone('select * from github_users as guser').then( (result:any)=>{
    result.forEach(function(table) {
      table.location = 'Lisbon';
      console.log(table.id,'-',table.login,'-',table.name,'-',table.company,'-',table.twitter_username,'-',table.location);
    });
    process.exit(0);

  }
  );
}

const InsertUser = ()=>{
  db.tx(transacao => {
    return transacao.oneOrNone(`SELECT EXISTS (  
                                  select null
                                    from pg_catalog.pg_class as cat
                                  inner join pg_catalog.pg_namespace as namesp on namesp.oid = cat.relnamespace
                                  where namesp.nspname = 'public'
                                    and cat.relname = 'github_users'
                                    and cat.relkind = 'r'
                                  ) as ret`)
        .then( (data: any) => {
            if(data.ret != true) {
                return transacao.none('CREATE TABLE github_users (id BIGSERIAL, login TEXT, name TEXT, company TEXT, twitter_username TEXT, location TEXT ,PRIMARY key (id))')
            } else {
            console.log('Table already exists');
            return null;
            }
        });
  }).then(()=>{
    let username = process.argv[3];
    if (username != null){
      db.tx(transacao2 => {
        return transacao2.oneOrNone('SELECT EXISTS ( select null from github_users as guser where guser.login = \''+username+'\' ) as ret',username)
        .then( (data2: any) => {
          if(data2.ret != true) {
            console.log('testestes: ',data2.ret);
            request({
              uri: 'https://api.github.com/users/'+username,
              headers: {
                    'User-Agent': 'Request-Promise'
                },
              json: true
            }).then((data: GithubUsers) => db.one(
                'INSERT INTO github_users (login,name,company,twitter_username,location) VALUES ($[login],$[name],$[company],$[twitter_username],$[location]) RETURNING id',data)
            ).then(({id}) => console.log(id))
            .then(() => process.exit(0));
          } else {
          console.log('User already exists');
          process.exit(0);
          }    
        })
      })
  
    } else{
      console.log('Username required');
      process.exit(0);
  
    }    
  })
};


switch (opt) {
  case 'list':
    listuser();
    break;
  case 'insert':
    InsertUser();
    break;
  default:
    console.log('Invalid Option!');
}